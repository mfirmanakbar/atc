<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('model_users');
		check_session();
	}

	public function index()
    {

        $this->load->library('pagination');
        // $config['base_url']     = base_url().'index.php/users/index/';
        $config['base_url']     = site_url().'users/index/';
        $config['total_rows']   = $this->model_users->tampilkan_data()->num_rows();
        $config['per_page']     = 10;
        $this->pagination->initialize($config);
        $data_paging         = $this->pagination->create_links();
        $halaman                = $this->uri->segment(3);
        $halaman                = $halaman==''?0:$halaman;
        $data = array(
            'title' => 'Users',
            'desc_link' => 'Admin > Users',
            'paging' => $data_paging,
            'record' => $this->model_users->tampilkan_data_paging($halaman,$config['per_page']),
            );
        $this->template->load('template_admin/contents', 'template_admin/component/page_users',$data);
	}

    function input()
    {
        if(isset($_POST['btnSave']))
        {
            $addUsername = $this->input->post('addUsername');
            $addPassword = $this->input->post('addPassword');
            $addJabatan = $this->input->post('addJabatan');
            $hasil = $this->model_users->cek_username($addUsername);
            if($hasil==1)
            {
                $this->session->set_flashdata('pesan_users_gagal', "username tidak boleh sama.");
                redirect(site_url('users'));
            }
            else
            {
                $data_input     = array(
                                'id'=>'',
                                'username'=> $addUsername,
                                'password'=> md5($addPassword),
                                'jabatan' => $addJabatan,
                                );
                $this->model_users->input($data_input);
                $this->session->set_flashdata('pesan_users_sukses', "Data Username <b><i>".$username."</i></b> berhasil disimpan.");
                redirect(site_url('users'));
            }
        }
    }

    function edit()
    {
        if(isset($_POST['btnEdit']))
        {
            $editUsername = $this->input->post('editUsername');
            $editPassword = $this->input->post('editPassword');
            $editJabatan = $this->input->post('editJabatan');
            $data_update = array(
                            'password'=> md5($editPassword),
                            'jabatan'=> $editJabatan,
                            );
            $this->model_users->edit($data_update,$editUsername);
            $this->session->set_flashdata('pesan_users_sukses', "Data User:<b><i>".$editUsername."</i></b> berhasil diupdate.");
            redirect(site_url('users'));
        }
    }

    function delete()
    {
        $id = $this->uri->segment(3);
        $dec_id=str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
        $dec_id=$this->encrypt->decode($dec_id);
        $idnya=$dec_id;
        $this->model_users->delete($idnya);
		$this->session->set_flashdata('pesan_users_sukses', "Data User:<b><i>".$editUsername."</i></b> telah dihapus.");
        redirect(site_url('users'));
    }

    function change_password()
    {
        if(isset($_POST['btnCP']))
        {
            $cpUsername = $this->input->post('cpUsername');
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            $cnewPassword = $this->input->post('cnewPassword');

            $hasil = $this->model_users->login($cpUsername,$oldPassword);
            if ($hasil==1)
            {
                if($cnewPassword==$newPassword)
                {
                    $data_update = array(
                            'password'=> md5($cnewPassword),
                            );
                     $this->model_users->edit($data_update,$cpUsername);
                    $this->session->set_flashdata('pesan_users_sukses', "Password berhasil diubah.");
                    redirect(site_url('users'));
                }
                else
                {
                    $this->session->set_flashdata('pesan_users_gagal', "Konfirmasi Password tidak valid.");
                    redirect(site_url('users'));
                }
            }
            else
            {
                $this->session->set_flashdata('pesan_users_gagal', "Old Password salah.");
                redirect(site_url('users'));
            }
        }
    }



}
