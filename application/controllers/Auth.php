<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_users');
	}

	public function index()
	{
		if (isset($_POST['login']))
		{

			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$hasil = $this->model_users->login($username,$password);

			if ($hasil==1){
				// echo "<script>alert('ADA');</script>";
	            $jabatan = $this->model_users->get_jabatan($username);
				$this->session->set_userdata(
					array(
						'status_login' => 'oke',
						'atc_login_username' => $username,
						'atc_login_jabatan' => $jabatan,
						 ));
				redirect(site_url('dashboard'));
			}else{
				// echo "<script>alert('GAK ADA');</script>";
				$data = array(
				'title' => 'Adam Training Center',
				'desc_link' => 'Home > Login',
				'nilai' => '0',
				);
				$this->template->load('template_depan/contents', 'template_depan/component/page_login',$data);
			}

		}
		else
		{
			$data = array(
				'title' => 'Adam Training Center',
				'desc_link' => 'Home > Login',
				);
			chek_session_login();
			$this->template->load('template_depan/contents', 'template_depan/component/page_login',$data);
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function tamu(){
		$this->load->library('pagination');
		$config['base_url']     = site_url().'auth/tamu/';
		$config['total_rows']   = $this->model_users->tampilkan_data_tamu()->num_rows();
		$config['per_page']     = 2; //rows
		$this->pagination->initialize($config);
		$data_paging         = $this->pagination->create_links();
		$halaman                = $this->uri->segment(3);
		$halaman                = $halaman==''?0:$halaman;
		$data = array(
				'title' => 'Buku Tamu',
				'desc_link' => 'Home > Buku Tamu',
				'paging' => $data_paging,
				'record' => $this->model_users->tampilkan_data_paging_tamu($halaman,$config['per_page']),
				);
		$this->template->load('template_depan/contents', 'template_depan/component/page_tamu', $data);
	}

	function input_tamu()
	{
			if(isset($_POST['btnSave']))
			{
					$data_input     = array(
													'id'=>'',
													'nama'=> $this->input->post('txtnama'),
													'email'=> $this->input->post('txtemail'),
													'url'=> $this->input->post('txturl'),
													'pesan'=> $this->input->post('txtpesan'),
													);
					$this->model_users->input_tamu($data_input);
					redirect(site_url('auth/tamu'));
			}
	}

}
