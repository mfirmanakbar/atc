<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_ih extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('model_booking_ih');
		check_session();
	}
	
	public function index()
    {	
        $this->load->library('pagination');
        $config['base_url']     = site_url().'booking_ih/index/';
        $config['total_rows']   = $this->model_booking_ih->tampilkan_data()->num_rows();
        $config['per_page']     = 15;
        $this->pagination->initialize($config);
        $data_paging         = $this->pagination->create_links();      
        $halaman                = $this->uri->segment(3);
        $halaman                = $halaman==''?0:$halaman;
        $data = array(
            'title' => 'Booking',
            'desc_link' => 'Admin > Booking > In-House Training',
            'paging' => $data_paging,
            'record' => $this->model_booking_ih->tampilkan_data_paging($halaman,$config['per_page']),
            );
        $this->template->load('template_admin/contents', 'template_admin/component/page_booking_ih',$data);
	}

    public function done()
    {
        if(isset($_POST['btnEditDone']))
        { 
            $id = $this->input->post('id');
            $data_update = array(
                            'done'=> 'Y',
                            );
            $this->model_booking_ih->edit_done($data_update,$id);
            $this->session->set_flashdata('pesan_sukses_bih', "Done.");
            redirect(site_url('booking_ih'));
        }
    }

}