<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	function __construct(){
		parent::__construct();
		
		$this->load->model('model_event_ic');
		$this->load->model('model_event_rt');
		$this->load->model('model_booking');
		$this->load->model('model_users');
		$this->load->library('atc_send_email');	
	}
	
	// public function kei_tes()
	// {
	// 	$subjek = 'SubjekKei';
	// 	$isi = 'ISIjekKei';
	// 	$email = 'm.firman.aa@gmail.com';
	// 	$this->load->library('atc_send_email');
	// 	echo $this->atc_send_email->send_keimail($subjek,$isi,$email);
	// }

	public function index()
	{
		$ip = $_SERVER['REMOTE_ADDR']; // menangkap ip pengunjung
		$location = $_SERVER['PHP_SELF']; // menangkap server path
		
		$data_input     = array(
			'id'=>'',
			'location'=> $location,
			'ip'=> $ip,
			'date' => date("Y-m-d"),
			'time' => date("h:i:sa"),
			);
		$this->model_users->visitors($data_input);
		
		$data = array(
			'title' => 'Adam Training Center',
		);
		$this->template->load('template_depan/contents', 'template_depan/component/page_home', $data);
	}

	function unik_visitors()
	{
		//echo $this->model_users->visitors_unik();
	}
	
	function all_visitors()
	{
		//echo $this->model_users->visitors_all();
	}
	
	public function profile()
	{
		$data = array(
			'title' => 'Adam Training Center',
			'desc_link' => 'About Us > Profile',
		);
		$this->template->load('template_depan/contents', 'template_depan/component/page_profile', $data);
	}

	public function vission_and_misson()
	{
		$data = array(
			'title' => 'Adam Training Center',
			'desc_link' => 'About Us > Vision and Mission',
		);
		$this->template->load('template_depan/contents', 'template_depan/component/page_visimisi', $data);
	}

	public function professional()
	{
		$data = array(
			'title' => 'Adam Training Center',
			'desc_link' => 'About Us > Professional',
		);
		$this->template->load('template_depan/contents', 'template_depan/component/page_professional', $data);
	}

	public function event_regular()
	{
		//$this->load->model('model_event_rt');
        $this->load->library('pagination');
        $config['base_url']     = site_url().'home/event_regular/index/';
        $config['total_rows']   = $this->model_event_rt->tampilkan_data()->num_rows();
        $config['per_page']     = 15;
        $this->pagination->initialize($config);
        $data_paging            = $this->pagination->create_links();      
        $halaman                = $this->uri->segment(4);
        $halaman                = $halaman==''?0:$halaman;
        $data = array(
            'title' => 'Adam Training Center',
            'desc_link' => 'Admin > Events > Regular Training & Workshop',
            'paging' => $data_paging,
            'record' => $this->model_event_rt->tampilkan_data_paging($halaman,$config['per_page']),
            );
        $this->template->load('template_depan/contents', 'template_depan/component/page_event_regular',$data);
	}

	public function event_iht()
	{
		$data = array(
			'title' => 'Adam Training Center',
			'desc_link' => 'Event > In-House Training',
		);
		$this->template->load('template_depan/contents', 'template_depan/component/page_event_iht', $data);
	}

	public function event_ic()
	{
	//$this->load->model('model_event_ic');
        $this->load->library('pagination');
        $config['base_url']     = site_url().'home/event_ic/index/';
        $config['total_rows']   = $this->model_event_ic->tampilkan_data()->num_rows();
        $config['per_page']     = 15;
        $this->pagination->initialize($config);
        $data_paging            = $this->pagination->create_links();      
        $halaman                = $this->uri->segment(4);
        $halaman                = $halaman==''?0:$halaman;
        $data = array(
            'title' => 'Adam Training Center',
            'desc_link' => 'Admin > Intensive Course',
            'paging' => $data_paging,
            'record' => $this->model_event_ic->tampilkan_data_paging($halaman,$config['per_page']),
            );
        $this->template->load('template_depan/contents', 'template_depan/component/page_event_ic',$data);

	}	

	public function contactus()
	{
		$data = array(
			'title' => 'Adam Training Center',
			'desc_link' => 'Contact Us',
		);
		
		$this->template->load('template_depan/contents', 'template_depan/component/page_contactus', $data);
	}

	public function contactus_input()
	{
		if(isset($_POST['btnContactus_input']))
		{
 			$subject = $this->input->post('subject');			
			$tgl = date("Y-m-d");
 			$name = $this->input->post('name');
 			$email = $this->input->post('email');
 			$phone = $this->input->post('phone');
 			$message = $this->input->post('message');

	        $subjek = 'Pemberitahuan Pesan Masuk';
			$isi = "
			<h3 align='center'>".$subject."</h3>
	        <b>Date of Message :</b> ".$tgl." <br><hr>
	        <b>Client Name :</b> ".$name." <br><hr>
	        <b>Client Email :</b> ".$email." <br><hr>
	        <b>Client Phone Number :</b> ".$phone." <br><hr>
	        <b>Client Email Address :</b> ".$message." <br><hr>
			";
			$this->load->library('atc_send_email');
	        echo $this->atc_send_email->send_now($subjek,$isi);
			$this->session->set_flashdata('pesan_sukses_contactus', "Message has been sent, we will contact you later. Thank you.");
            redirect(site_url('contact-us'));
            
    	}
	}
	
	function booking_ic()
	{
		if(isset($_POST['btnBookingIC']))
		{
 			$ic_id = $this->input->post('id');
 			$ic_title = $this->input->post('judul');
 			$tgl = date("Y-m-d");
 			$name = $this->input->post('name');
 			$company = $this->input->post('company');
 			$phone = $this->input->post('phone');
 			$email = $this->input->post('email');
 			$peserta = $this->input->post('peserta');

            $data_input = array(
		                'id'=>'',
		                'ic_id'=> $ic_id,
		                'ic_title'=> $ic_title,
		                'tgl'=> $tgl,
		                'name'=> $name,
		                'company'=> $company,
		                'phone'=> $phone,
		                'email'=> $email,
		                'peserta'=> $peserta,
		                );
						
            $this->model_booking->input_ic($data_input);
            $subjek = 'Pemberitahuan Booking Event';
    		$isi = "
			<h3 align='center'>Event Title : ".$ic_title."</h3>
            <hr><b>Event Category :</b> Intensive Course <br><hr>
            <b>Date of Booking :</b> ".$tgl." <br><hr>
            <b>Client Name :</b> ".$name." <br><hr>
            <b>Client Company :</b> ".$company." <br><hr>
            <b>Client Phone Number :</b> ".$phone." <br><hr>
            <b>Client Email Address :</b> ".$email." <br><hr>
            <b>Total Participants :</b> ".$peserta." <br><hr>
			";
			
            echo $this->atc_send_email->send_now($subjek,$isi);

            $this->session->set_flashdata('pesan_sukses_booking_intensive', "Booking has been successful.. we will contact you later. Thank you.");
            redirect(site_url('intensive-course'));
		}
	}

	function booking_ih()
	{
		if(isset($_POST['btnBookingIH']))
		{
 			$title = $this->input->post('title');
 			$tgl = date("Y-m-d");
 			$name = $this->input->post('name');
 			$company = $this->input->post('company');
 			$phone = $this->input->post('phone');
 			$email = $this->input->post('email');
			$peserta = $this->input->post('peserta');

            $data_input = array(
		                'id'=>'',
		                'title'=> $title,
		                'tgl'=> $tgl,
		                'name'=> $name,
		                'company'=> $company,
		                'phone'=> $phone,
		                'email'=> $email,
		                'peserta'=> $peserta,
		                );
			//$this->load->model('model_booking');

            $this->model_booking->input_ih($data_input);

            $subjek = 'Pemberitahuan Booking Event';
    		$isi = "
			<h3 align='center'>Event Title : ".$title."</h3>
            <hr><b>Event Category :</b> In-House Training <br><hr>
            <b>Date of Booking :</b> ".$tgl." <br><hr>
            <b>Client Name :</b> ".$name." <br><hr>
            <b>Client Company :</b> ".$company." <br><hr>
            <b>Client Phone Number :</b> ".$phone." <br><hr>
            <b>Client Email Address :</b> ".$email." <br><hr>
			<b>Total Participants :</b> ".$peserta." <br><hr>
			";
			
			//$this->load->library('atc_send_email');
            echo $this->atc_send_email->send_now($subjek,$isi);

            $this->session->set_flashdata('pesan_sukses_booking_inhouse', "Booking has been successful.. we will contact you later. Thank you.");
            redirect(site_url('in-house-training'));
		}
	}

	function booking_rt()
	{
		if(isset($_POST['btnBookingRT']))
		{
 			$rt_id = $this->input->post('id');
 			$rt_title = $this->input->post('judul');
 			$tgl = date("Y-m-d");
 			$name = $this->input->post('name');
 			$company = $this->input->post('company');
 			$phone = $this->input->post('phone');
 			$email = $this->input->post('email');
			$peserta = $this->input->post('peserta');

            $data_input = array(
		                'id'=>'',
		                'rt_id'=> $rt_id,
		                'rt_title'=> $rt_title,
		                'tgl'=> $tgl,
		                'name'=> $name,
		                'company'=> $company,
		                'phone'=> $phone,
		                'email'=> $email,
		                'peserta'=> $peserta,
		                );
			//$this->load->model('model_booking');

            $this->model_booking->input_rt($data_input);

            $subjek = 'Pemberitahuan Booking Event';
    		$isi = "
			<h3 align='center'>Event Title : ".$rt_title."</h3>
            <hr><b>Event Category :</b> Regular Training & Workshop <br><hr>
            <b>Date of Booking :</b> ".$tgl." <br><hr>
            <b>Client Name :</b> ".$name." <br><hr>
            <b>Client Company :</b> ".$company." <br><hr>
            <b>Client Phone Number :</b> ".$phone." <br><hr>
            <b>Client Email Address :</b> ".$email." <br><hr>
			<b>Total Participants :</b> ".$peserta." <br><hr>
			";
			
			//$this->load->library('atc_send_email');
            echo $this->atc_send_email->send_now($subjek,$isi);

            $this->session->set_flashdata('pesan_sukses_booking_regular', "Booking has been successful.. we will contact you later. Thank you.");
            redirect(site_url('regular-training-workshop'));
		}
	}

	function services_rt()
	{
        $data = array(
            'title' => 'Adam Training Center',
            'desc_link' => 'Services > Regular Training/Workshop',
            );
        $this->template->load('template_depan/contents', 'template_depan/component/page_service_rt',$data);
	}

	function services_ih()
	{
        $data = array(
            'title' => 'Adam Training Center',
            'desc_link' => 'Services > In-House Training',
            );
        $this->template->load('template_depan/contents', 'template_depan/component/page_service_ih',$data);
	}

	function services_ic()
	{
        $data = array(
            'title' => 'Adam Training Center',
            'desc_link' => 'Services > Intensive Course',
            );
        $this->template->load('template_depan/contents', 'template_depan/component/page_service_ic',$data);
	}


}
