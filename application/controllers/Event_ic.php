<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Event_ic extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('model_event_ic');
		check_session();
	}

	public function index()
    {
        $this->load->library('pagination');
        $config['base_url']     = site_url().'event_ic/index/';
        $config['total_rows']   = $this->model_event_ic->tampilkan_data()->num_rows();
        $config['per_page']     = 15;
        $this->pagination->initialize($config);
        $data_paging            = $this->pagination->create_links();
        $halaman                = $this->uri->segment(3);
        $halaman                = $halaman==''?0:$halaman;
        $data = array(
            'title' => 'Events',
            'desc_link' => 'Admin > Events > Intensive Course',
            'paging' => $data_paging,
            'record' => $this->model_event_ic->tampilkan_data_paging($halaman,$config['per_page']),
            );
        $this->template->load('template_admin/contents', 'template_admin/component/page_event_ic',$data);
	}

    function input()
    {
        if(isset($_POST['btnSave']))
        {
            $judul = $this->input->post('judul');
            $tgl = $this->input->post('tgl');
            $tempat = $this->input->post('tempat');
            $data_input     = array(
                            'id'=>'',
                            'judul'=> $judul,
                            'tgl'=> $tgl,
                            'tempat' => $tempat,
                            );
            $this->model_event_ic->input($data_input);
            $this->session->set_flashdata('pesan_event_ic_sukses', "Data event berhasil disimpan.");
            redirect(site_url('event_ic'));
        }
    }

    function edit()
    {
        if(isset($_POST['btnEdit']))
        {
            $id = $this->input->post('id');
            $judul = $this->input->post('judul');
            $tgl = $this->input->post('tgl');
            $tempat = $this->input->post('tempat');
            $data_input     = array(
                            'judul'=> $judul,
                            'tgl'=> $tgl,
                            'tempat' => $tempat,
                            );
            $this->model_event_ic->edit($data_input,$id);
            $this->session->set_flashdata('pesan_event_ic_sukses', "Data event berhasil diubah.");
            redirect(site_url('event_ic'));
        }
    }

    function delete()
    {
        $id = $this->uri->segment(3);
        $dec_id=str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
        $dec_id=$this->encrypt->decode($dec_id);
        $idnya=$dec_id;
        $this->model_event_ic->delete($idnya);
				$this->session->set_flashdata('pesan_event_ic_sukses', "Data event berhasil dihapus.");
        redirect(site_url('event_ic'));
    }


}
