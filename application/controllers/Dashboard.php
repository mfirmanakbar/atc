<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('model_users');
		check_session();
	}
	
	public function index(){
        $visitors_get = $this->model_users->visitors_get();
        $visit_get = $this->model_users->visit_get();
		$data = array(
				'title' => 'Dashboard',
				'desc_link' => 'Admin > Dashboard',
				'visitors_get' => $visitors_get,
				'visit_get' => $visit_get,
				'nilai' => '0',
				);
		$this->template->load('template_admin/contents', 'template_admin/component/page_dashboard',$data);
	}

	function tes()
	{
        /*$visitors_get = $this->model_users->visitors_get();
        $visit_get = $this->model_users->visit_get();
        echo $visitors_get."/".$visit_get;*/
	}

}