	<?php include "element_desc_link.php"; ?>


	<div class="container">
		<div style="padding-top:40px;">
			<div class="row">
				<div class="row" style="margin-left:10px;margin-right:10px;">
					<div class="col-lg-3">
						
						<?php $this->load->view('template_depan/component/element_events'); ?>						
					</div>
					<div class="col-lg-9">
					<div class="panel panel-default">
					  <div class="panel-body">
					    <h3>In-House Training</h3>
					    <hr>
					    <p align="justify"><b>In-house Training</b>
					    merupakan pelatihan pajak yang desain dan materinya  disesuaikan dengan kebutuhan client untuk menjawab permasalahan  perpajakan yang dihadapi perusahaan, yang dapat diselenggarakan  di dalam lingkungan perusahaan atau tempat lain yang disediakan perusahaan. In-house Training menjadikan waktu dan sumber daya perusahaan menjadi lebih efisien dan efektif.
						</p>
					    <hr>
					    <br>
					    <h4>Order Event:</h4>
					    <hr>

						<?php
						if ($this->session->flashdata('pesan_sukses_booking_inhouse') == TRUE)
						{
						?>
							<div class="alert alert-success">
								<a class="close" data-dismiss="alert">&times;</a> <strong>Success! </strong>
								<?php echo $this->session->flashdata('pesan_sukses_booking_inhouse'); ?>
							</div>
						<?php
						}
						?>
						    <?php echo form_open('home/booking_ih');?>
								<div class="form-group">
									<label>*Event Title</label>
									<input type="text" required class="form-control" name="title" placeholder="Event Title">
								</div>
								<div class="form-group">
									<label>*Name</label>
									<input type="text" required class="form-control" name="name" placeholder="Name">
								</div>
								<div class="form-group">
									<label>*Company</label>
									<input type="text" required class="form-control" name="company" placeholder="Company">
								</div>
								<div class="form-group">
									<label>Total Participants</label>
									<input type="text" class="form-control" name="peserta" placeholder="Total Participants">
								</div>
								<div class="form-group">
									<label>*Phone Number</label>
									<input type="text" required class="form-control" name="phone" placeholder="Phone Number">
								</div>
								<div class="form-group">
									<label>*Email Address</label>
									<input type="email" required class="form-control" name="email" placeholder="Email Address">
								</div>
									
								<div class="modal-footer">
									<button type="submit" name="btnBookingIH" class="btn btn-primary">ORDER EVENT</button>
								</div>
							</form>			
						</div>

					  </div>
					</div>

					</div>
				</div>
			</div>			
		</div>
	</div>