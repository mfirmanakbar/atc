	
	<?php include "element_desc_link.php"; ?>

	<div class="container">
		<div style="padding-top:40px;">
			<div class="row">
				<!--CU-->
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<div class="well well-sm" style="background-color:#ffffff;">
								<?php
								if ($this->session->flashdata('pesan_sukses_contactus') == TRUE)
								{
								?>
									<div class="alert alert-success">
										<a class="close" data-dismiss="alert">&times;</a> <strong>Success! </strong>
										<?php echo $this->session->flashdata('pesan_sukses_contactus'); ?>
									</div>
								<?php
								}
								?>
								<?php echo form_open('home/contactus_input');?>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="name">
												Name</label>
											<input type="text" class="form-control" name="name" placeholder="Enter name" required="required" />
										</div>
										<div class="form-group">
											<label for="email">
												Email Address</label>
											<div class="input-group">
												<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
												</span>
												<input type="email" class="form-control" name="email" placeholder="Enter email" required="required" /></div>
										</div>
										<div class="form-group">
											<label for="subject">
												Subject</label>
											<select name="subject" required class="form-control" required="required">
												<option value="na" selected="">Choose One:</option>
												<option value="Service">General Customer Service</option>
												<option value="Suggestions">Suggestions</option>
												<option value="Product Support">Product Support</option>
											</select>
										</div>

										<div class="form-group">
											<label for="email">
												Phone Number</label>
											<div class="input-group">
												<span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span>
												</span>
												<input required type="text" class="form-control" name="phone" placeholder="Enter Phone Number" required="required" /></div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="name">
												Message</label>
											<textarea required name="message" class="form-control" rows="12" cols="25" required="required"
												placeholder="Message"></textarea>
										</div>
									</div>
									<div class="col-md-12">
										<button type="submit" class="btn btn-mycustom pull-right" name="btnContactus_input">
											Send Message</button>
									</div>
								</div>
								</form>
							</div>
						</div>
						<div class="col-md-4">
							<form>
							<legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>
							<address>
								<strong>Adam Training Center</strong><br>
								Plaza Sentral Lt.3<br>
								Jl. Jend. Sudirman Kav. 47<br>
								Jakarta 12930<br>
								<b>Phone :</b>+6221-5785-1111<br>
								<b>Phone :</b>+6221-5785-3307<br>
								<b>Fax :</b>+6221-5785-3308
							</address>
							<address>
								<strong>Email</strong><br>
								<a href="mailto:info@adamtrainingcenter.com">info@adamtrainingcenter.com</a><br><br>
								<strong>Website</strong><br>
								<a href="http://www.adamtrainingcenter.com">www.adamtrainingcenter.com</a>
							</address>
							</form>
						</div>
					</div>
				</div>
				<!--CU-->
			</div>
		</div>
	</div>
