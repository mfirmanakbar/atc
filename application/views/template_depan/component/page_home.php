
	<!--ini slider 1000 = 1 detik -->
	<div id="carousel-example-generic" class="carousel slide slidernya" style="" data-ride="carousel"  data-interval="20000">
		<ol class="carousel-indicators hidden-xs">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
			<div class="item active">
				<p align="center"><img class="img-responsive img-full" src="<?php echo base_url('assets/img/slider/a.png');?>" width="100%" alt=""></p>
			</div>
			<div class="item">
				<p align="center"><img class="img-responsive img-full" src="<?php echo base_url('assets/img/slider/b.png');?>" width="100%" alt=""></p>
			</div>
			<div class="item">
				<p align="center"><img class="img-responsive img-full" src="<?php echo base_url('assets/img/slider/c.png');?>" width="100%" alt=""></p>
			</div>
		</div>
		<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"><span class="icon-prev"></span></a>
		<a class="right carousel-control" href="#carousel-example-generic" data-slide="next"><span class="icon-next"></span></a>
	</div>
	<!--ini slider-->

	<div style="background-color:#A67C00;margin-bottom:10px;">
		<div class="container" style="padding-bottom:50px;padding-top:10px">
			<div class="row">

			  			<h2 align="center" style="color:#ffffff">Welcome to Adam Training Center</h2>
				<p style="padding-right:20px;padding-left:20px;font-size:18px;color:#ffffff;" align="center">

<i style="color:#ffffff" class="fa fa-quote-left fa-2x"></i>
Menjadikan para alumnus ATC memahami pengetahuan perpajakan yang baik dan benar sesuai peraturan perpajakan yang berlaku dan terkini, Menjadikan para alumnus ATC mampu melaksanakan hak dan memenuhi kewajiban perpajakan sesuai dengan peraturan perundang-undangan perpajakan, Menjadikan para alumnus ATC berkualitas dan mumpuni berstandar internasional.
<i class="fa fa-quote-right fa-2x"></i>
<br>
				</p>
			</div>
		</div>
	</div>

	<div style="background-color:#F6F6F6">
		<div class="container" style="margin-top:-30px">
			<br><br>
			<h2 align="center" style="color:#A67C00"> Events </h2><hr><br><br>
			<div class="row">
				<div class="col-md-4">
					<p align="center">
					<a href="<?php echo site_url('regular-training-workshop');?>">
					<img class="img-responsive" src="<?php echo base_url('assets/img/events/rt.jpg');?>" alt="">
					</a>
					</p>
				</div>
				<div class="col-md-4">
					<p align="center">
					<a href="<?php echo site_url('in-house-training');?>">
					<img class="img-responsive" src="<?php echo base_url('assets/img/events/iht.jpg');?>" alt="">
					</a>
					</p>
				</div>
				<div class="col-md-4">
					<p align="center">
					<a href="<?php echo site_url('intensive-course');?>">
					<img class="img-responsive" src="<?php echo base_url('assets/img/events/ic.jpg');?>" alt="">
					</a>
					</p>
				</div>
			</div><br><hr>
		</div>
	</div>


	<div class="container" style="padding-top:40px;padding-bottom:40px;">

			<div class="col-md-4">
				<div class="list-group">
					<a class="list-group-item" style="background-color:#A67C00;color:#ffffff;border-color:#A67C00">
						<h4 align="center">EVENTS</h4>
					</a>
					<a class="list-group-item" href="<?php echo site_url('regular-training-workshop');?>">
						<i class="fa fa-check-square-o"></i>&nbsp;
						Regular Training / Workshop
					</a>
					<a class="list-group-item" href="<?php echo site_url('in-house-training');?>">
						<i class="fa fa-check-square-o"></i>&nbsp;
						In-House Training
					</a>
					<a class="list-group-item" href="<?php echo site_url('intensive-course');?>">
						<i class="fa fa-check-square-o"></i>&nbsp;
						Intensive Course
					</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="list-group">
					<a class="list-group-item" style="background-color:#A67C00;color:#ffffff;border-color:#A67C00">
						<h4 align="center">TAX RULES [pajak.go.id]</h4>
					</a>
					<a class="list-group-item" href="http://pajak.go.id/content/peraturan-menteri-keuangan-nomor-16pmk0102016">
						<i class="fa fa-life-ring"></i>&nbsp;
						Keputusan Dirjen Pajak Nomor: KEP-49/PJ/2016
					</a>
					<a class="list-group-item" href="http://pajak.go.id/content/peraturan-dirjen-pajak-nomor-47pj2015">
						<i class="fa fa-life-ring"></i>&nbsp;
						Peraturan Menteri Keuangan Nomor 16/PMK.010/2016
					</a>
					<a class="list-group-item" href="http://pajak.go.id/content/peraturan-dirjen-pajak-nomor-42pj2015">
						<i class="fa fa-life-ring"></i>&nbsp;
						Peraturan Dirjen Pajak Nomor PER-47/PJ/2015
					</a>
					<a class="list-group-item" href="http://pajak.go.id/content/peraturan-dirjen-pajak-nomor-20pj2015">
						<i class="fa fa-life-ring"></i>&nbsp;
						Peraturan Dirjen Pajak Nomor PER-42/PJ/2015
					</a>
					<a class="list-group-item" href="http://pajak.go.id/content/peraturan-dirjen-pajak-nomor-45pj2013">
						<i class="fa fa-life-ring"></i>&nbsp;
						Peraturan Dirjen Pajak Nomor PER-20/PJ/2015
					</a>
					<a class="list-group-item btn btn-primary" href="http://pajak.go.id/peraturan-perpajakan">MORE...</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="list-group">
					<a class="list-group-item" style="background-color:#A67C00;color:#ffffff;border-color:#A67C00">
						<h4 align="center">KURS [pajak.go.id]</h4>
					</a>
					<a class="list-group-item">
						<p align="center">
							No. 19/KM.10/2016 tanggal 26 April 2016. Berlaku 27 April 2016 - 3 Mei 2016
						</p>
					</a>
					<a class="list-group-item" >
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Currency</th>
									<th>Rp</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>USD</td>
									<td>13,173.00</td>
								</tr>
								<tr>
									<td>AUD</td>
									<td>10,209.25</td>
								</tr>
								<tr>
									<td>EUR</td>
									<td>14,857.71</td>
								</tr>
							</tbody>
						</table>
					</a>
					<a class="list-group-item btn btn-primary" href="http://fiskal.kemenkeu.go.id/dw-kurs-db.asp">MORE...</a>
				</div>
			</div>
	</div>
