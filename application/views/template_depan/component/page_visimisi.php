	<?php include "element_desc_link.php"; ?>
	
	<div class="container">
		<div style="padding-top:40px;">
			<div class="row">
				<div class="row">
				    <h3 style="margin-top:0px;" align="center">Vision and Mission</h3>
				    <hr>
					<div class="col-lg-3 hidden-xs">
						
					<?php $this->load->view('template_depan/component/element_aboutus'); ?>
						
					</div>
					<div class="col-lg-9">
							<div class="panel panel-default" style="margin-left:10px;margin-right:10px;">
							  <div class="panel-body">
								  		

							  		<p align="justify">
										<h4 align="" style="color: #A67C00; margin-left: 25px;">VISI ATC</h4>
										<ul>
											<li><p align="justify">Menjadi Lembaga Training Perpajakan domestik dan internasional yang berkualitas, independen dan terpercaya  di Indonesia.</p></li>
										</ul>
										<br><hr>
										<h4 align="" style="color: #A67C00; margin-left: 25px;">MISI ATC</h4>
										<ul>
											<li><p align="justify">Menjadikan para alumnus ATC   memahami pengetahuan perpajakan yang baik dan benar sesuai peraturan perpajakan yang berlaku dan terkini.</p></li>

											<li><p align="justify">Menjadikan para alumnus ATC mampu  melaksanakan   hak dan  memenuhi  kewajiban perpajakan sesuai dengan  peraturan perundang-undangan perpajakan.</p></li>

											<li><p align="justify">Menjadikan para alumnus ATC berkualitas dan mumpuni berstandar  internasional.</p></li>
										</ul>
										<br>
							  		</p>

						  		</div>
					  		</div>


						</div>
				  </div>
			</div>
		</div>
	</div>