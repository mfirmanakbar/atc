	<?php include "element_desc_link.php"; ?>
	
	<div class="container">
		<div style="padding-top:40px;">
			<div class="row">
				<div class="row">
				    <h3 style="margin-top: 0px;" align="center">OUR PROFESSIONAL</h3>
				    <hr>
					<div class="col-lg-3 hidden-xs">
						
					<?php $this->load->view('template_depan/component/element_aboutus'); ?>
						
					</div>
					<div class="col-lg-9">

					  		<div class="panel panel-default" style="margin-left:10px;margin-right:10px;">
							  <div class="panel-body">
							  		<div class="row">
								  		<div class="col-md-3">
											<p align="center"><img width="" src="<?php echo base_url('assets/img/about/dhiky1.jpg');?>" alt="our professinal" class="img-responsive img-thumbnail"></p>
										</div>
								  		<div class="col-md-9">
											<p align="justify"><b>
											Dhiky Kurniawan, SE, MM</b><br>
Merupakan seorang mantan Penelaah Keberatan dan Pemeriksa Pajak yang mempunyai pengalaman  bekerja  lebih dari 12 tahun  di Direktorat Jenderal Pajak (DJP). Alumni Diploma III Sekolah Tinggi Akuntansi Negara dan S2 jururan Manajemen di Universitas Budi luhur Jakarta.
<br><br>
Selama bekerja di DJP, beliau juga pengajar Brevet A, B dan C di beberapa lembaga training perpajakan. Pada  tahun 2013 memutuskan  untuk  mengundurkan diri dari institusi DJP. Selama tahun 2013 - 2015, beliau  fokus   sebagai seorang instruktur pelatihan perpajakan di Lembaga Manajemen Formasi. Kesibukan beliau saat ini adalah instruktur pajak pada event seminar, workshop dan training perpajakan serta In-house Training di berbagai perusahaan.
										</p>
										</div>
							  		</div>
						  		</div>
					  		</div>
							
							<div class="panel panel-default" style="margin-left:10px;margin-right:10px;">
							  <div class="panel-body">
							  		<div class="row">
								  		<div class="col-md-3">
											<p align="center"><img width="" src="<?php echo base_url('assets/img/about/adam.jpg');?>" alt="our professinal" class="img-responsive img-thumbnail"></p>
										</div>
								  		<div class="col-md-9">
											<p align="justify"><b>
											Adam Damili, SE, Ak, M.Si, BKP</b><br>

Beliau merupakan Managing Partner pada Kantor Konsultan Pajak Adam Damili & Partners (AD&P). Kesibukan beliau saat ini adalah  menangani kasus-kasus Multinational Corporation dalam proses pemeriksaan,  keberatan maupun banding.
<br><br>
Beliau lulusan D III STAN dan Alumnus S2 Fisip Universitas Indonesia (UI) Salemba Jakarta.  Belajar perpajakan internasional di South Korea (2010) dan Transfer Pricing di Netherland (2015). Setelah mengabdi pada negara (DJP) selama 15 tahun, tahun 2010 mengundurkan diri dengan hormat  dari DJP dan memutuskan menjadi  Konsultan Pajak.

											</p>
										</div>
							  		</div>
						  		</div>
					  		</div>

					  		<div class="panel panel-default" style="margin-left:10px;margin-right:10px;">
							  <div class="panel-body">
							  		<div class="row">
								  		<div class="col-md-3">
											<p align="center"><img width="" src="<?php echo base_url('assets/img/about/indra.jpg');?>" alt="our professinal" class="img-responsive img-thumbnail"></p>
										</div>
								  		<div class="col-md-9">
											<p align="justify"><b>
											Indrayagus Slamet, SST, Ak, M.Ak, BKP</b><br>
Profesi sekarang adalah Dosen perpajakan S2 Magister Akuntansi UI Salemba, S1 FE Ekstensi UI Salemba, dan S1 FE UI Depok. Kesibukan beliau selain mengajar adalah sebagai konsultan pajak berizin praktek Brevet C dari IKPI dan DJP dan sebagai Quality Controller for Tax Skill pada sebuah perusahaan PMA Japan.
<br><br>
Beliau lulusan D IV STAN dan Magister Akuntansi UI, dan belajar perpajakan internasional di South Korea (2010) dan  Transfer Pricing di Netherland (2015). Setelah mengabdi pada negara (DJP) selama 15 tahun, pada awal 2011 mengundurkan diri dengan hormat dari DJP dan memutuskan menjadi Dosen dan Konsultan Pajak khusus Transfer Pricing di Kantor Konsultan Pajak Adam Damili & Partners.											
										</div><p>
							  		</div>
						  		</div>
					  		</div>

					  		<div class="panel panel-default" style="margin-left:10px;margin-right:10px;">
							  <div class="panel-body">
							  		<div class="row">
								  		<div class="col-md-3">
											<p align="center"><img width="" src="<?php echo base_url('assets/img/about/oki.jpg');?>" alt="our professinal" class="img-responsive img-thumbnail"></p>
										</div>
								  		<div class="col-md-9">
											<p align="justify"><b>Okky Makmuri, SE, MM</b><br>
Merupakan alumnus UGM jurusan Akuntansi dan Perpajakan  Tahun 2001. Mempunyai pengalaman kerja di berbagai perusahaan seperti perusahaan tambang dan Kantor Akuntan Publik. Kesibukan beliau saat ini di Kantor Konsultan Pajak Adam Damili & Partners adalah  menangani audit, tax compliance, tax review dan Transfer Pricing.
											</p>
										</div>
							  		</div>
						  		</div>
					  		</div>
						</div>
				  </div>
			</div>
		</div>
	</div>