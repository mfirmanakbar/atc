	<?php include "element_desc_link.php"; ?>  

    <?php 
        if(isset($nilai))
        {
            if($nilai==0)
            {
                echo '<div class="alert alert-danger" role="alert">
                <p align="center"><strong>Sorry! </strong>Username or Password was incorrect, please try again.</p></div>';
            }
        }
    ?>

	<div style="padding-bottom: 30px;">
		<div class="card card-container">		
		    <h1 align="center">Welcome</h1>
		    <hr>
			<?php echo form_open('atc-login-admin'); ?>
				<div class="form-group">
					<input type="text" class="form-control" name="username" placeholder="username..."required>
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="Password" required>
				</div>
				<button type="submit" name="login" class="btn btn-mycustom btn-block">Login</button>
			</form>	
	    </div>
    </div>

<?php 
	if(($this->session->userdata('atc_login_username'))!=""){
		redirect(site_url('dashboard'));
	}
 ?>
