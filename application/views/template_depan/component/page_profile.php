	<?php include "element_desc_link.php"; ?>
	
	<div class="container">
		<div style="padding-top:40px;">
			<div class="row">
				<div class="row">
				    <h3 align="center" style="margin-top:0px;">COMPANY PROFILE</h3>
				    <hr>
					<div class="col-lg-3 hidden-xs">
						
					<?php $this->load->view('template_depan/component/element_aboutus'); ?>
						
					</div>
					<div class="col-lg-9">
							<div class="panel panel-default" style="margin-left:10px;margin-right:10px;">
							  <div class="panel-body">

						  		<p align="justify">
Adam Training Center (ATC) merupakan kumpulan profesional  yang terdiri dari para  praktisi perpajakan baik dari kalangan mantan Direktorat Jenderal Pajak, Konsultan Pajak dan Dosen Perpajakan. Dengan pengalaman yang dimiliki oleh para profesional, kami yakin dapat memberikan training perpajakan serta konsultasi perpajakan yang berkualitas dan independen.
<br><br>
ATC dibentuk sesuai dengan semangat jiwa para profesional  untuk berbagi ilmu pengetahuan perpajakan kepada dunia usaha, pegawai, akademisi dan masyarakat pada umumnya.
<br><br>
ATC adalah sebuah lembaga training center yang bertujuan memberikan pengetahuan perpajakan dengan baik dan benar sesuai  peraturan perpajakan terkini sehingga para alumnus dapat melaksanakan hak dan memenuhi kewajiban perpajakan sesuai dengan ketentuan peraturan perundang-undangan perpajakan.
						  		</p>

						  		</div>
					  		</div>


						</div>
				  </div>
			</div>
		</div>
	</div>