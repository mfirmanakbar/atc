	<?php include "element_desc_link.php"; ?>

	<div class="container">
		<div style="padding-top:40px;">
			<div class="row" style="margin-left:10px;margin-right:10px;">
				<div class="col-lg-3">
					
					<?php $this->load->view('template_depan/component/element_events'); ?>						
				</div>
				<div class="col-lg-9">

				<div class="panel panel-default">
					<div class="panel-body">
						<h3>Intensive Course</h3>
						<hr>
						<p align="justify"><b>Intensive Course</b>
Merupakan pelatihan pajak  khusus atas materi tertentu dengan pembatasan jumlah peserta dan  waktu yang telah ditentukan yang diselenggarakan di ruang training ATC. Intensive Course bertujuan agar peserta dapat memahami secara mendalam dan seksama dari konsep dasar hingga penerapan lanjutan ketentuan  peraturan perundang-undangan perpajakan yang berlaku.
						 </p>
					</div>
				</div>

				<?php
				if ($this->session->flashdata('pesan_sukses_booking_intensive') == TRUE)
				{
				?>
					<div class="alert alert-success">
						<a class="close" data-dismiss="alert">&times;</a> <strong>Success! </strong>
						<?php echo $this->session->flashdata('pesan_sukses_booking_intensive'); ?>
					</div>
				<?php
				}
				?>

				<?php
					$no=1+$this->uri->segment(3);
				    foreach ($record->result() as $r)
				    {
				?>
				<!--BEGIN MODAL BOOKING-->
					<div class="modal fade" id="modal_booking_ic<?php echo $r->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								
								<?php echo form_open('home/booking_ic');?>

									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" ><i class="fa fa-plus"></i> Booking Event Intensive Course</h4>
									</div>
									<div class="modal-body form">
										<div class="form-group">
											<label>*Event Title</label>
											<input name="id" type="hidden" value="<?php echo $r->id; ?>"></input>
											<input name="judul" type="hidden" value="<?php echo $r->judul; ?>"></input>
											<input disabled value="<?php echo $r->judul; ?>" class="form-control">
										</div>
										<div class="form-group">
											<label>*Name</label>
											<input type="text" class="form-control" required name="name" placeholder="Name">
										</div>
										<div class="form-group">
											<label>*Company</label>
											<input type="text" class="form-control" required name="company" placeholder="Company">
										</div>
										<div class="form-group">
											<label>Total Participants</label>
											<input type="text" class="form-control" name="peserta" placeholder="Total Participants">
										</div>
										<div class="form-group">
											<label>*Phone Number</label>
											<input type="text" class="form-control" required name="phone"placeholder="Phone Number">
										</div>
										<div class="form-group">
											<label>*Email Address</label>
											<input type="email" class="form-control" required name="email" placeholder="Email Address">
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										<button type="submit" name="btnBookingIC" class="btn btn-success">Order Event</button>
									</div>

								</form>
							</div>
						</div>
					</div>
				<!--END MODAL BOOKING-->



				<div class="panel panel-default">
				  <div class="panel-body">
				    <div class="row">
				    	<div class="col-md-8">
				    		 <b><?php echo $no; ?>. Event Title : </b><?php echo $r->judul; ?>
				    	</div>
				    	<div class="col-md-4 text-right">
					    	<button data-toggle="modal" data-target="#modal_booking_ic<?php echo $r->id; ?>" type="button" class="btn btn-primary">
					    	<i class="fa fa-calendar-check-o"></i> Booking Now</button>
				    	</div>
				  	</div>
				    <hr>
				    <div class="row">
				    	<div class="col-md-6">
				    		<i class="fa fa-calendar"></i> Date: <br>
				    		<?php echo $r->tgl; ?>
				    	</div>
				    	<div class="col-md-6">
				    		<i class="fa fa-map-marker"></i> Address: <br>
				    		<?php echo $r->tempat; ?>
				    	</div>
				    </div>
				  </div>
				</div>

				<?php 
						$no++;
					}
					echo $paging;
				?>

				</div>
			</div>
		</div>
	</div>
	