<?php include "element_desc_link.php"; ?>


<div style="padding-bottom: 30px;">

  <div class="container">

<br/><br/>
<div class="panel panel-default">
  <div class="panel-body">

	<?php echo form_open('auth/input_tamu');?>
    <div class="form-group">
      <label for="exampleInputEmail1">Name</label>
      <input type="text" class="form-control" name="txtnama" placeholder="Name" required="required">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" class="form-control" name="txtemail" placeholder="Email" required="required">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">URL</label>
      <input type="text" class="form-control" name="txturl" placeholder="URL/Website">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Message</label>
      <textarea class="form-control" rows="3" name="txtpesan" required="required"></textarea>
    </div>
    <button type="submit" name="btnSave" class="btn btn-default">Submit</button>
  </form>

  </div>
</div>

    <table class="table table-stiped table-bordered?>">
      <thead>
        <tr>
          <th class="text-center">No</th>
          <th class="text-center">Date</th>
          <th class="text-center">Name</th>
          <th class="text-center">Email</th>
          <th class="text-center">URL</th>
          <th class="text-center">Message</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $no=1+$this->uri->segment(3);
            foreach ($record->result() as $r)
            {
        ?>
        <tr>
          <td class="text-center"><?php echo $no; ?></td>
          <td class="text-center"><?php echo $r->time; ?></td>
          <td class="text-center"><?php echo $r->nama; ?></td>
          <td class="text-center"><?php echo $r->email; ?></td>
          <td class="text-center"><?php echo $r->url; ?></td>
          <td class="text-center"><?php echo $r->pesan; ?></td>
        </tr>
        <?php
          $no++;
            }
        ?>
      </tbody>
    </table>

    <?php
        echo $paging;
    ?>

  </div>

</div>
