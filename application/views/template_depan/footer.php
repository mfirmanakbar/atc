
	<div style="background-color:#a67c00">
		<div class="container" style="padding-bottom:20px;padding-top:20px;">
			<div class="row">
				<div class="row footer1" style="margin-left:10px;margin-right:10px;">				
					<div class="col-sm-8">
						<h4>Adam Training Center</h4><hr>
						<p align="justify">
							<a>
Adam Training Center (ATC) merupakan kumpulan profesional yang terdiri dari para praktisi perpajakan baik dari kalangan mantan Direktorat Jenderal Pajak, Konsultan Pajak dan Dosen Perpajakan. Dengan pengalaman yang dimiliki oleh para profesional, kami yakin dapat memberikan training perpajakan serta konsultasi perpajakan yang berkualitas dan independen.
<br><br>
ATC dibentuk sesuai dengan semangat jiwa para profesional untuk berbagi ilmu pengetahuan perpajakan kepada dunia usaha, pegawai, akademisi dan masyarakat pada umumnya.
<br><br>
ATC adalah sebuah lembaga training center yang bertujuan memberikan pengetahuan perpajakan dengan baik dan benar sesuai peraturan perpajakan terkini sehingga para alumnus dapat melaksanakan hak dan memenuhi kewajiban perpajakan sesuai dengan ketentuan peraturan perundang-undangan perpajakan. 
						</a></p>
					</div>
					<div class="col-sm-4">
						<h4>INFORMATION</h4><hr>
						<p><a href="#"><i class="fa fa-map-marker"></i> &nbsp; <b>Address:</b>
						<br>Plaza Sentral Lt.3 Jl. Jend. Sudirman Kav. 47 Jakarta, 12930.
						</a></p>
						<p><a href="#"><i class="fa fa-phone"></i> &nbsp; <b>Phone:</b> +6221-5785-1111</a></p>						
						<p><a href="#"><i class="fa fa-phone"></i> &nbsp; <b>Phone:</b> +6221-5785-3307</a></p>
						<p><a href="#"><i class="fa fa-fax"></i> &nbsp; <b>Fax:</b> +6221-5785-3308</a></p>
						<p><a href="#"><i class="fa fa-envelope-o"></i> &nbsp; <b>Email:</b> info@adamtrainingcenter.com</a></p>						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- <div style="background-color:#936f04; color:#ffffff;">
		<div class="container">
			<div class="row footer2">
				<br><br>
				<p align="center">
					<a href="#">Home&nbsp; | &nbsp;</a>
					<a href="#">Profile&nbsp; | &nbsp;</a>
					<a href="#">Vision & Mission&nbsp; | &nbsp;</a>
					<a href="#">Professional&nbsp; | &nbsp;</a>
					<a href="#">Regular Training&nbsp; | &nbsp;</a>
					<a href="#">In-House Training&nbsp; | &nbsp;</a>
					<a href="#">Intensive Course&nbsp; | &nbsp;</a>
					<a href="#">Contact Us]&nbsp;</a>
					<a href="<?php echo site_url('atc-login-admin'); ?>">[Login]&nbsp;</a>

					<br><br>
					ATC &copy; 2016, All Rights Reserved |
					<a href="http://www.adamtrainingcenter.com">www.adamtrainingcenter.com</a>
					<br><br><br>
				</p>
			</div>
		</div>
	</div> -->

	<div style="background-color:#1a1a1a; color:#ffffff;">
		<br>
		<p align="center" class="footer2">
			<a href="#">Home&nbsp; | &nbsp;</a>
			<a href="#">Profile&nbsp; | &nbsp;</a>
			<a href="#">Vision & Mission&nbsp; | &nbsp;</a>
			<a href="#">Professional&nbsp; | &nbsp;</a>
			<a href="#">Regular Training&nbsp; | &nbsp;</a>
			<a href="#">In-House Training&nbsp; | &nbsp;</a>
			<a href="#">Intensive Course&nbsp; | &nbsp;</a>
			<a href="#">Contact Us&nbsp; | &nbsp;</a>
			<a href="<?php echo site_url('atc-login-admin'); ?>">Login</a>
			<br><br>
			ATC &copy; 2016, All Rights Reserved |
			<a href="http://www.adamtrainingcenter.com">www.adamtrainingcenter.com</a>
		</p>	
		<br>
	</div>

	
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.easing.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/scrolling-nav.js');?>"></script>
    <script>
	$(document).ready(function(e){
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var param = $(this).attr("href").replace("#","");
		var concept = $(this).text();
		$('.search-panel span#search_concept').text(concept);
		$('.input-group #search_param').val(param);
		});
	});
	</script>
