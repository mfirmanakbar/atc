
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
			<div class="navbar-header page-scroll">
                    <a class="navbar-brand" href="<?php echo base_url();?>"><img width="" src="<?php echo base_url('assets/img/logoa.png');?>" alt="Adam Training Center"></a><br /><br />
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<div class="top-bar">
						<div class="pull-right">
							<a><i class="fa fa-phone"></i> <span class="colored-text">
							+6221-5785-1111</span> </a>
							<a><i class="fa fa-envelope"></i> <span class="colored-text">
							info@adamtrainingcenter.com</span> </a>
						</div>
					</div>
					<ul class="nav navbar-nav navbar-right" style="padding-bottom:30px;margin-top:-10px;">
						<li><a class="padding-bottom:30px;margin-top:-10px;" href="<?php echo site_url();?>">HOME</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">ABOUT US &nbsp;&nbsp;<b class="caret"></b></a>
							<ul class="dropdown-menu">
							<li><a href="<?php echo site_url('profile');?>">Profile</a></li>
							<li><a href="<?php echo site_url('vission-and-misson');?>">Vision & Mission</a></li>
							<li><a href="<?php echo site_url('professional');?>">Professional</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">OUR SERVICE &nbsp;&nbsp;<b class="caret"></b></a>
							<ul class="dropdown-menu">
							<li><a href="<?php echo site_url('services-regular');?>">Regular Training/Workshop</a></li>
							<li><a href="<?php echo site_url('services-in-house'); ?>">In-House Training</a></li>
							<li><a href="<?php echo site_url('services-intensive-course'); ?>">Intensive Course</a></li>
							<li><a href="#">Tax Compliance </a></li>
							<li><a href="#">Tax Review </a></li>
							<li><a href="#">Tax Consultancy </a></li>
							<li><a href="#">Tax Assistance </a></li>
							<li><a href="#">Tax Objection </a></li>
							<li><a href="#">Litigation </a></li>
							<li><a href="#">Transfer Pricing Documentation </a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">EVENTS & BOOKING&nbsp;&nbsp;<b class="caret"></b></a>
							<ul class="dropdown-menu">
							<li><a href="<?php echo site_url('regular-training-workshop'); ?>">Regular Training/Workshop</a></li>
							<li><a href="<?php echo site_url('in-house-training'); ?>">In-House Training</a></li>
							<li><a href="<?php echo site_url('intensive-course'); ?>">Intensive Course</a></li>
							</ul>
						</li>
						<li><a class="page-scroll" href="<?php echo site_url('contact-us'); ?>">CONTACT US</a></li>
						<li><a class="page-scroll" href="<?php echo site_url('atc-tamu'); ?>">BUKU TAMU</a></li>
					</ul>
				</div>
            </div>
        </div>
    </nav>
