<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="title" Content="ATC | Adam Training Center">
	
	<meta name="description" content="Adam Training Center (ATC) merupakan kumpulan profesional yang terdiri dari para praktisi perpajakan baik dari kalangan mantan Direktorat Jenderal Pajak, Konsultan Pajak dan Dosen Perpajakan. Dengan pengalaman yang dimiliki oleh para profesional, kami yakin dapat memberikan training perpajakan serta konsultasi perpajakan yang berkualitas dan independen. 
ATC dibentuk sesuai dengan semangat jiwa para profesional untuk berbagi ilmu pengetahuan perpajakan kepada dunia usaha, pegawai, akademisi dan masyarakat pada umumnya. 
ATC adalah sebuah lembaga training center yang bertujuan memberikan pengetahuan perpajakan dengan baik dan benar sesuai peraturan perpajakan terkini sehingga para alumnus dapat melaksanakan hak dan memenuhi kewajiban perpajakan sesuai dengan ketentuan peraturan perundang-undangan perpajakan.">

	<meta name="keywords" content="Adam Training Center, adamtrainingcenter, ATC, Training Pajak, Pelatihan Pajak, Tax Treaty, Seminar Pajak">
	<meta name="robot" content="index,follow">
	<meta name="language" content="indonesia">
	
    <title>ATC | <?php echo $title;?></title>
	<link rel="alternate" hreflang="id" href="http://adamtrainingcenter.com/">
	<link rel="alternate" hreflang="x-default" href="http://adamtrainingcenter.com/" />
	<link rel="icon" href="<?php echo base_url('assets/img/icon.png');?>">
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/scrolling-nav.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/font-awesome-4.5.0/css/font-awesome.min.css');?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js');?>"></script>
	
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php $this->load->view('template_depan/header'); ?>
	<script>
  (function() {
    var cx = '001258068968116924617:ywk62oymo9i';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
	<?php echo $contents; ?>
	<?php $this->load->view('template_depan/footer'); ?>

</body>
</html>
