<?php include "element_desc_link.php" ?>

<br><br>

<div class="container">
	<div class="row">
		<div class="col-md-3 hidden-xs ">
			<?php include "element_menu.php";?>
		</div>
		<div class="col-md-9">
			
			<div class="panel panel-default">
			  <div class="panel-body">
			  	<h3>
			  	<i class="fa fa-tachometer"></i> 
			  	Dashboard
			  	</h3><hr>

<!-- 				<div class="alert alert-info" role="alert">
				  <a href="#" class="alert-link">Update Website v.1.2</a>
				</div>
 -->
				<div class="list-group">
				  <!-- <a href="#" class="list-group-item active">
						<h4><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Visit Website</h4> 
				  </a>
 -->				  
				  <a href="#" class="list-group-item">

					<div class="row">
						<div class="col-lg-3">
							<b> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Total Visitors 
						</div>
						<div class="col-lg-9">
						:&nbsp;&nbsp; <?php echo number_format($visitors_get); ?> </b> users on internet
						</div>
					</div>

				  </a>
				 
				  <a href="#" class="list-group-item">
				  	<div class="row">
						<div class="col-lg-3">
							<b> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Total Visit 
						</div>
						<div class="col-lg-9">
						:&nbsp;&nbsp; <?php echo number_format($visit_get); ?> </b> times
						</div>
					</div>
				  </a>

				</div>

			  </div>
			</div>
			
		</div>
	</div>
</div>
