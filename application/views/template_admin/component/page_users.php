<?php include "element_desc_link.php" ?>

<br><br>

<div class="container">
	<div class="row">
		<div class="col-md-3 hidden-xs ">
			<?php include "element_menu.php";?>
		</div>
		<div class="col-md-9">


		<!--BEGIN MODAL ADD USER-->
			<div class="modal fade" id="modal_add_user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						
						<?php echo form_open('users/input');?>

							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" ><i class="fa fa-plus"></i> Add New User</h4>
							</div>
							<div class="modal-body form">
								<div class="form-group">
									<label>Username</label>
									<input required type="text" name="addUsername" class="form-control" placeholder="Username">
								</div>
								<div class="form-group">
									<label>Password</label>
									<input required type="password" name="addPassword" class="form-control" placeholder="Password">
								</div>
								<div class="form-group">
									<label>Jabatan</label><br>
									<label class="radio-inline">
									<input required type="radio" name="addJabatan" value="SUPER ADMIN"> SUPER ADMIN
									</label><br>
									<label class="radio-inline">
									<input required type="radio" name="addJabatan" value="ADMIN"> Admin
									</label>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit" name="btnSave" class="btn btn-success">Save</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		<!--END MODAL ADD USER-->


			<div class="panel panel-default">
			  <div class="panel-body">
			    <h3>
			  	<i class="fa fa-users"></i> 
			  	User
			  	</h3>
			  	<hr>
			  	<?php  
			  		$usernamess= $this->session->userdata('atc_login_username');
			  		$jabatanss= $this->session->userdata('atc_login_jabatan');
		  		?>
			  	<b>Welcome</b> <?php echo $usernamess." (<i>".$jabatanss."</i>)"; ?>
			  	
			  	<a type="button" class="btn btn-primary btn-xs" data-toggle="modal" 
			  	data-target="#modal_change_password">
				<i class="fa fa-key"></i> Change Password</a>


				<!--BEGIN MODAL CHANGE PASSWORD-->
					<div class="modal fade" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								
								<?php echo form_open('users/change_password');?>

									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" ><i class="fa fa-pencil"></i> Change Password [<?php echo $usernamess; ?>]</h4>
									</div>
									<div class="modal-body form">
										<div class="form-group">
											<label>Username</label>
											<input disabled value="<?php echo $usernamess; ?>" required type="text" class="form-control" placeholder="Username">
											<input type="hidden" value="<?php echo $usernamess; ?>" required type="text" name="cpUsername" class="form-control" placeholder="Username">
										</div>
										<div class="form-group">
											<label>Old Password</label>
											<input required type="password" name="oldPassword" class="form-control" placeholder="Password">
										</div>
										<hr>
										<div class="form-group">
											<label>New Password</label>
											<input required type="password" name="newPassword" class="form-control" placeholder="Password">
										</div>
										<div class="form-group">
											<label>Confirm Password</label>
											<input required type="password" name="cnewPassword" class="form-control" placeholder="Password">
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										<button type="submit" name="btnCP" class="btn btn-success">Save</button>
									</div>

								</form>
							</div>
						</div>
					</div>
				<!--END MODAL CHANGE PASSWORD-->
			  	
			  	<hr>
			  	
			  	<button type="button" class="btn btn-success btn-xs <?php if($jabatanss!="SUPER ADMIN"){echo "hidden";} ?>" data-toggle="modal" data-target="#modal_add_user">
			  	<i class="fa fa-plus"></i> Add User</button><br><br>

				<?php 
				if($this->session->flashdata('pesan_users_gagal') == TRUE)
					{
				?>
					<div class="alert alert-danger">
						<a class="close" data-dismiss="alert">&times;</a> <strong>Info! </strong>
						<?php echo $this->session->flashdata('pesan_users_gagal'); ?>
					</div>
				<?php
				}
				else if ($this->session->flashdata('pesan_users_sukses') == TRUE)
				{
				?>
					<div class="alert alert-success">
						<a class="close" data-dismiss="alert">&times;</a> <strong>Info! </strong>
						<?php echo $this->session->flashdata('pesan_users_sukses'); ?>
					</div>
				<?php
					}
				?>

				<table class="table table-stiped table-bordered <?php if($jabatanss!="SUPER ADMIN"){echo "hidden";} ?>">
					<thead>
						<tr>
							<th class="text-center">No</th>
							<th class="text-center">Username</th>			
							<th class="text-center">Jabatan</th>
							<th class="text-center">Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no=1+$this->uri->segment(3);
						    foreach ($record->result() as $r)
						    {
						?>
						<tr>
							<td class="text-center"><?php echo $no; ?></td>
							<td class="text-center"><?php echo $r->username; ?></td>
							<td class="text-center"><?php echo $r->jabatan; ?></td>

							<!--BEGIN MODAL EDIT USER-->
								<div class="modal fade" id="modal_edit_user<?php echo $r->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											
											<?php echo form_open('users/edit');?>

												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" ><i class="fa fa-pencil"></i> Edit User [<?php echo $r->username; ?>]</h4>
												</div>
												<div class="modal-body form">
													<div class="form-group">
														<label>Username</label>
														<input disabled value="<?php echo $r->username; ?>" required type="text" class="form-control" placeholder="Username">
														<input type="hidden" value="<?php echo $r->username; ?>" required type="text" name="editUsername" class="form-control" placeholder="Username">
													</div>
													<div class="form-group">
														<label>New Password</label>
														<input required type="password" name="editPassword" class="form-control" placeholder="Password">
													</div>
													<div class="form-group">
														<label>Jabatan</label><br>
														<label class="radio-inline">
														<input required type="radio" name="editJabatan" value="SUPER ADMIN"> SUPER ADMIN
														</label><br>
														<label class="radio-inline">
														<input required type="radio" name="editJabatan" value="ADMIN"> ADMIN
														</label>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
													<button type="submit" name="btnEdit" class="btn btn-success">Save</button>
												</div>

											</form>
										</div>
									</div>
								</div>
							<!--END MODAL EDIT USER-->

							<td class="text-center">
							  	<a type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_edit_user<?php echo $r->id; ?>">
			  					<i class="fa fa-pencil"></i> Edit User</a>
								<?php 
									$total_segments=$this->uri->total_segments();
									$enc_id=$this->encrypt->encode($r->id);
									$enc_id=str_replace(array('+', '/', '='), array('-', '_', '~'), $enc_id);
									$idnya = $enc_id;
									if($total_segments==1){
										echo '<a href=" users/delete/'.$idnya.' " class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> Delete</a>';
									}elseif($total_segments==2){
										echo '<a href=" ../delete/'.$idnya.' " class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> Delete</a>';
									}elseif($total_segments==3){
										echo '<a href=" ../delete/'.$idnya.' " class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> Delete</a>';
									}
								?>

							</td>
						</tr>
						<?php 
							$no++;
							}
						?>
					</tbody>
				</table>

				<?php
					if($jabatanss=="SUPER_ADMIN")
					{
						echo $paging;
					}

				?>

			  </div>
			</div>

		</div>
	</div>
</div>
