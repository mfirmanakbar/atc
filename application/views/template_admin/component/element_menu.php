
	<div class="list-group">
		<!-- <a align="center" style="background-color:#A67C00;color:#ffffff;border-color:#A67C00" class="list-group-item active">MENU</a> -->
		<a class="list-group-item" href="<?php echo site_url();?>">
			<i class="fa fa-home"></i> Home
		</a>
		<a class="list-group-item" href="<?php echo site_url('dashboard');?>">
			<i class="fa fa-tachometer"></i> Dashboard
		</a>
		<a class="list-group-item" href="<?php echo site_url('booking_rt');?>">
			<i class="fa fa-bookmark"></i> B.Regular
		</a>
		<a class="list-group-item" href="<?php echo site_url('booking_ih');?>">
			<i class="fa fa-bookmark"></i> B.In-House
		</a>
		<a class="list-group-item" href="<?php echo site_url('booking_ic');?>">
			<i class="fa fa-bookmark"></i> B.Intensive
		</a>
		<a class="list-group-item" href="<?php echo site_url('users');?>">
			<i class="fa fa-users"></i> Users
		</a>
		<a class="list-group-item" href="<?php echo site_url('event_rt');?>">
			<i class="fa fa-archive"></i> E.Regular Training
		</a>
		<a class="list-group-item" href="<?php echo site_url('event_ic');?>">
			<i class="fa fa-archive"></i> E.Intensive Course
		</a>
		<a class="list-group-item" href="#">
			<i class="fa fa-book"></i> T.Rules
		</a>
		<a class="list-group-item" href="#">
			<i class="fa fa-book"></i> T.Kurs
		</a>
		<a class="list-group-item" href="<?php echo site_url('atc-logout-admin');?>">
			<i class="fa fa-sign-out"></i> Logout
		</a>
	</div>
	<br><br>