<?php include "element_desc_link.php" ?>

<br><br>

<div class="container">
	<div class="row">
		<div class="col-md-3 hidden-xs ">
			<?php include "element_menu.php";?>
		</div>
		<div class="col-md-9">
			
			<div class="panel panel-default">
			  <div class="panel-body">
			  	<h3><i class="fa fa-bookmark"></i> Booking In-House Training</h3>
			  	<hr>

			  	<?php 
			  	if ($this->session->flashdata('pesan_sukses_bih') == TRUE)
				{
				?>
					<div class="alert alert-success">
						<a class="close" data-dismiss="alert">&times;</a> <strong>Info! </strong>
						<?php echo $this->session->flashdata('pesan_sukses_bih'); ?>
					</div>
				<?php
					}
				?>

				<table class="table table-stiped table-bordered">
					<thead>
						<tr>
							<!-- <th class="text-center">No</th> -->
							<th class="text-center">E.In-House</th>			
							<th class="text-center">Date</th>
							<th class="text-center">Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no=1+$this->uri->segment(3);
						    foreach ($record->result() as $r)
						    {
						?>

						<tr class="<?php if($r->done==""){echo "alert alert-danger";} ?>" >
							<!-- <td class="text-center"><?php echo $no; ?></td> -->
							<td class="text-center"><?php echo $r->title; ?></td>
							<td class="text-center"><?php echo $r->tgl; ?></td>
							<td class="text-center">
								<a type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_booking_ic<?php echo $r->id; ?>">
								<i class="fa fa-eye"></i> View</a>
							</td>

							<!--BEGIN MODAL EDIT USER-->
								<div class="modal fade" id="modal_booking_ic<?php echo $r->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" ><i class="fa fa-eye"></i> Booking Regular Training</h4>
												</div>
												<div class="modal-body form">
													<b>Event Title:</b><br><?php echo $r->title; ?><br><hr>
													<b>Booking Date:</b><br><?php echo $r->tgl; ?><br><hr>
													<b>Name:</b><br><?php echo $r->name; ?><br><hr>
													<b>Company:</b><br><?php echo $r->company; ?><br><hr>
													<b>Total Participants:</b><br><?php echo $r->peserta; ?><br><hr>
													<b>Phone Number:</b><br><?php echo $r->phone; ?><br><hr>
													<b>Email Address:</b><br><?php echo $r->email; ?><br>
												</div>
												<div class="modal-footer">
													<?php echo form_open('booking_ih/done');?>
													<input name="id" type="hidden" value="<?php echo $r->id; ?>"/>
													<button type="submit" name="btnEditDone" class="btn btn-success">Done</button>
													</form>
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>

										</div>
									</div>
								</div>
							<!--END MODAL EDIT USER-->

						</tr>
						<?php 
							$no++;
							}
						?>
					</tbody>
				</table>
				<?php
					echo $paging;
				?>


			  </div>
			</div>

		</div>
	</div>
</div>
