<?php include "element_desc_link.php" ?>

<br><br>

<div class="container">
	<div class="row">
		<div class="col-md-3 hidden-xs ">
			<?php include "element_menu.php";?>
		</div>
		<div class="col-md-9">


			<div class="panel panel-default">
			  <div class="panel-body">
			    <h3><i class="fa fa-archive"></i> Intensive Course</h3>
			  	<hr>
			  	<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal_add_event">
		    	<i class="fa fa-plus"></i> Add Event</button>
		    	<br><br>

				<?php 
				if ($this->session->flashdata('pesan_event_ic_sukses') == TRUE)
				{
				?>
					<div class="alert alert-success">
						<a class="close" data-dismiss="alert">&times;</a> <strong>Info! </strong>
						<?php echo $this->session->flashdata('pesan_event_ic_sukses'); ?>
					</div>
				<?php
				}
				?>
				
				<?php
					$no=1+$this->uri->segment(3);
				    foreach ($record->result() as $r)
				    {
				?>


				<!--BEGIN MODAL EDIT-->
					<div class="modal fade" id="modal_edit_event<?php echo $r->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								
								<?php echo form_open('event_ic/edit');?>

									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" ><i class="fa fa-plus"></i> Edit Event</h4>
									</div>
									<div class="modal-body form">
										<div class="form-group">
											<label>Event Title</label>
											<input name="id" type="hidden" value="<?php echo $r->id; ?>"></input>
											<input value="<?php echo $r->judul; ?>" required type="text" name="judul" class="form-control" placeholder="Event Title">
										</div>
										<div class="form-group">
											<label>Date</label>
											<input value="<?php echo $r->tgl; ?>" required type="text" name="tgl" class="form-control" placeholder="Date">
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea required name="tempat" class="form-control" placeholder="Address" rows="3"><?php echo $r->tempat; ?></textarea>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										<button type="submit" name="btnEdit" class="btn btn-success">Save</button>
									</div>

								</form>
							</div>
						</div>
					</div>
				<!--END MODAL EDIT-->



				<div class="panel panel-default" style="border-color:#A67C00;">
				  <div class="panel-body">
				    <div class="row">
				    	<div class="col-md-8">
				    		 <b><?php echo $no; ?>. Event Title : </b><?php echo $r->judul; ?>
				    	</div>
				    	<div class="col-md-4 text-right">
					    	<button data-toggle="modal" data-target="#modal_edit_event<?php echo $r->id; ?>" type="button" class="btn btn-primary btn-xs">
					    	<i class="fa fa-pencil"></i> Edit</button>
							<?php 
								$total_segments=$this->uri->total_segments();
								$enc_id=$this->encrypt->encode($r->id);
								$enc_id=str_replace(array('+', '/', '='), array('-', '_', '~'), $enc_id);
								$idnya = $enc_id;
								if($total_segments==1){
									echo '<a href=" event_ic/delete/'.$idnya.' " class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> Delete</a>';
								}elseif($total_segments==2){
									echo '<a href=" ../delete/'.$idnya.' " class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> Delete</a>';
								}elseif($total_segments==3){
									echo '<a href=" ../delete/'.$idnya.' " class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> Delete</a>';
								}
							?>
				    	</div>
				  	</div>
				    <hr>
				    <div class="row">
				    	<div class="col-md-6">
				    		<i class="fa fa-calendar"></i> Date: <br>
				    		<?php echo $r->tgl; ?>
				    	</div>
				    	<div class="col-md-6">
				    		<i class="fa fa-map-marker"></i> Address: <br>
				    		<?php echo $r->tempat; ?>
				    	</div>
				    </div>
				  </div>
				</div>

				<?php 
						$no++;
					}
					echo $paging;
				?>


				<!--BEGIN MODAL ADD-->
					<div class="modal fade" id="modal_add_event" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								
								<?php echo form_open('event_ic/input');?>

									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" ><i class="fa fa-plus"></i> Add New Event</h4>
									</div>
									<div class="modal-body form">
										<div class="form-group">
											<label>Event Title</label>
											<input required type="text" name="judul" class="form-control" placeholder="Event Title">
										</div>
										<div class="form-group">
											<label>Date</label>
											<input required type="text" name="tgl" class="form-control" placeholder="Date">
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea required name="tempat" class="form-control" placeholder="Address" rows="3"></textarea>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										<button type="submit" name="btnSave" class="btn btn-success">Save</button>
									</div>

								</form>
							</div>
						</div>
					</div>
				<!--END MODAL ADD-->

			  </div>
			</div>

		</div>
	</div>
</div>
