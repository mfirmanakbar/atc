
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
			<div class="navbar-header page-scroll">
                    <a class="navbar-brand" href="<?php echo base_url();?>"><img width="" src="<?php echo base_url('assets/img/logoa.png');?>" alt="Adam Training Center"></a><br /><br />
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<div class="top-bar">
						<div class="pull-right">
							<a><i class="fa fa-phone"></i> <span class="colored-text">
							+62-21-5785-3307</span> </a> 
							<a><i class="fa fa-envelope"></i> <span class="colored-text">
							info@adamtrainingcenter.com</span> </a>
						</div>
					</div>
					<ul class="nav navbar-nav navbar-right" style="padding-bottom:30px;margin-top:-10px;">
						<li>
							<a class="padding-bottom:30px;margin-top:-10px;" href="<?php echo site_url();?>">
							<i class="fa fa-home"></i> Home
							</a>
						</li>
						<li>
							<a class="padding-bottom:30px;margin-top:-10px;" href="<?php echo site_url('dashboard');?>">
							<i class="fa fa-tachometer"></i> Dashboard
							</a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-bookmark"></i>
							Booking &nbsp;&nbsp;<b class="caret"></b></a>
							<ul class="dropdown-menu">
							<li><a href="<?php echo site_url('booking_rt');?>">Regular Training / Workshop</a></li>
							<li><a href="<?php echo site_url('booking_ih');?>">In-House Training</a></li>
							<li><a href="<?php echo site_url('booking_ic');?>">Intensive Course</a></li>
							</ul>
						</li>
						<li>							
							<a class="padding-bottom:30px;margin-top:-10px;" href="<?php echo site_url('users');?>"><i class="fa fa-users"></i> Users
							</a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-archive"></i>
							Events &nbsp;&nbsp;<b class="caret"></b></a>
							<ul class="dropdown-menu">
							<li><a href="<?php echo site_url('event_rt');?>">Regular Training / Workshop</a></li>
							<li><a href="<?php echo site_url('event_ic');?>">Intensive Course</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-book"></i>
							Tax &nbsp;&nbsp;<b class="caret"></b></a>
							<ul class="dropdown-menu">
							<li><a href="#">Tax Rules</a></li>
							<li><a href="#">Kurs</a></li>
							</ul>
						</li>
						<li>
							<a class="padding-bottom:30px;margin-top:-10px;" href="<?php echo base_url('atc-logout-admin');?>">
							<i class="fa fa-sign-out"></i> Logout
							</a>
						</li>
					</ul>
				</div>
            </div>
        </div>		
    </nav>

 