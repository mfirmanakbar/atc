<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
/*     if(($this->session->userdata('atc_login_username'))!="")
    { */
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php 
    echo $title;
    ?></title>
	<link rel="icon" href="<?php echo base_url('assets/img/icon.png');?>">
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/scrolling-nav.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/font-awesome-4.5.0/css/font-awesome.min.css');?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js');?>"></script>
	
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php $this->load->view('template_admin/header'); ?>
	<?php echo $contents; ?>
	<?php $this->load->view('template_depan/footer'); ?>

</body>
</html>

<?php 
/*     }else{
        redirect(base_url());
    } */
 ?>