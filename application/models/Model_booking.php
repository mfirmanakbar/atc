<?php if(!defined('BASEPATH')) exit('Hacking Attempt: Keluar dari sistem...!');

class Model_booking extends CI_Model
{

	function __construct(){
		parent::__construct();
	}

    function input_rt($data)
    {
        $this->db->insert('tb_booking_rt',$data);
    }

    function input_ih($data)
    {
        $this->db->insert('tb_booking_ih',$data);
    }

    function input_ic($data)
    {
        $this->db->insert('tb_booking_ic',$data);
    }

}

 ?>