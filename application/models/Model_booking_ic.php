<?php if(!defined('BASEPATH')) exit('Hacking Attempt: Keluar dari sistem...!');

class Model_booking_ic extends CI_Model
{

	function __construct(){
		parent::__construct();
	}

	function tampilkan_data()
	{
		return $this->db->get('tb_booking_ic');
	}

	function tampilkan_data_paging($halaman,$list)
	{
		return $this->db->query("select * from tb_booking_ic order by id desc limit $halaman, $list");
	}

	function edit_done($data,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('tb_booking_ic',$data);
    }
	
	function delete_booking($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_booking_ic');
    }
}

 ?>