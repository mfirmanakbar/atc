<?php if(!defined('BASEPATH')) exit('Hacking Attempt: Keluar dari sistem...!');

class Model_booking_ih extends CI_Model
{

	function __construct(){
		parent::__construct();
	}

	function tampilkan_data()
	{
		return $this->db->get('tb_booking_ih');
	}

	function tampilkan_data_paging($halaman,$list)
	{
		return $this->db->query("select * from tb_booking_ih order by id desc limit $halaman, $list");
	}

	function edit_done($data,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('tb_booking_ih',$data);
    }

	function delete_booking($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_booking_ih');
    }
}

 ?>